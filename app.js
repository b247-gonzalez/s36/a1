const express = require('express');
const mongoose = require('mongoose');
const taskRoute = require('./routes/taskRoute');

const app = express();
const port = 8000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://lng-athens:BVgvPPJIEu6qjm8V@zuitt-bootcamp.oquvder.mongodb.net/s36-todo?retryWrites=true&w=majority",
{
    useNewUrlParser: true,
    useUnifiedTopology: true
});

let db = mongoose.connection;

db.on("error", console.error.bind(console, "Connection error"));
db.on("open", () => console.log(
`Connected to MongoDB Atlas
Connected to Zuitt-Bootcamp database
Connected to s35-activity collection`
));

app.use("/tasks", taskRoute);

app.listen(port, () => console.log(`Server is now running at http://localhost:${port}`));