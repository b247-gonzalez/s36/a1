const Task = require('../models/task');

module.exports.getAllTask = () => {
    return Task.find({}).then((result) => {
        return result;
    });
}

module.exports.createTask = (requestBody) => {
    let newTask = new Task({
        name: requestBody.name
    });
    
    return newTask.save().then((task, err) => {
        if (err) {
            console.error(err);
            return false;
        }
        else {
            return task;
        }
    });
}

module.exports.deleteTask = (taskId => {
    return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
        if (err) {
            console.error(err);
            return false;
        }
        else {
            return removedTask;
        }
    });
});

module.exports.updateTask = ((taskId, newContent) => {
    return Task.findById(taskId).then((rslt, err) => {
        if (err) {
            console.error(err);
            return false;
        }
        
        rslt.name = newContent.name;

        return rslt.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.error(saveErr);
                return false;
            }
            else {
                return updatedTask;
            }
        });
    });
});

// Activity
module.exports.getTask = ((taskId) => {
    return Task.findById(taskId).then((rslt) => {
        return rslt;
    });
});

module.exports.updateTaskToComplete = ((taskId) => {
    return Task.findById(taskId).then((rslt, err) => {
        if (err) {
            console.error(err);
            return false;
        }
        rslt.status = "completed";

        return rslt.save().then((updatedTask, saveErr) => {
            if (saveErr) {
                console.error(saveErr);
                return false;
            }
            else {
                return updatedTask;
            }
        });
    });
});